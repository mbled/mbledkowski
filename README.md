### Hi, welcome on my profile 👋
#### About me:
- Front-end / Full-stack Developer
- Open-source Enthusiast
- Colemak Layout User
- Arch Linux User
- Underground Music Listener

#### Profiles:
* Codepen: [@mbled](https://codepen.io/mbled)
* Stack Overflow: [Maciej Błędkowski](https://stackoverflow.com/users/10492295)
* LinkedIn: [@mbled](https://linkedin.com/in/mbled)
##### Social Media
* Instagram: [@mbledkowski](https://instagram.com/mbledkowski)
* Discord: h4xx0r#6139
##### Other
* Codewars: [@mbled](https://codewars.com/users/mbled)
* TryHackMe: [@mble](https://tryhackme.com/p/mble)
* SoloLearn: [Maciej Błędkowski](https://www.sololearn.com/Profile/5637768)

#### Websites:
* Maciej Błędkowski's website: [mble.dk](https://mble.dk)
* keycomp - website about mechanical keyboard switches: [keycomp.co](https://keycomp.co) (Work in progress!)
* Minecraft (Server) Package Manager: [mpm.gg](https://mpm.gg)
* PluGet - Minecraft plugin API: [pluget.net](https://pluget.net)
* Antipixels archive: [antipixel.art](https://antipixel.art)
* MediaWiki alternative / influencers wiki: [neu.wiki](https://neu.wiki) / [influ.wiki](https://influ.wiki)

#### Stack:
Front-end | Back-end | DevOps
--------- | -------- | ------
HTML / Pug / CSS / Sass / SCSS | Node.JS / Deno / Bun | PostgreSQL / MongoDB
TailwindCSS / DaisyUI | TypeScript / JavaScript, Go | Docker / Docker Compose
Vue / Nuxt | Express.js / Nest.js | Oracle Cloud Infrastructure / Google Cloud Platform
React / Next | Jest / Puppeteer | Linux / Unix (Ubuntu / Debian, Fedora / RHEL, Arch Linux, NixOS, macOS)
Preact / Fresh | Vite / Webpack / Babel |
-- | OpenAPI |

##### Also
Rust and Rocket.rs

#### Other cool people:
<table><tbody><tr><td><a href="https://octo-ring.com/"><img src="https://octo-ring.com/static/img/widget/top.png" width="99%" alt="Octo Ring logo" align="top"></a><br><a href="https://octo-ring.com/p/mbledkowski/prev"><img src="https://octo-ring.com/static/img/widget/prev.png" width="33%" alt="previous" align="top" title="previous profile"></a><a href="https://octo-ring.com/p/mbledkowski/random"><img src="https://octo-ring.com/static/img/widget/random.png" width="33%" alt="random" align="top" title="random profile"></a><a href="https://octo-ring.com/p/mbledkowski/next"><img src="https://octo-ring.com/static/img/widget/next.png" width="33%" alt="next" align="top" title="next profile"></a><br><a href="https://octo-ring.com/"><img src="https://octo-ring.com/static/img/widget/bottom.png" width="99%" alt="check out other GitHub profiles in the Octo Ring" align="top"></a></td></tr></tbody></table> 
